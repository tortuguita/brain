# Run server
# To start this after reboot, add this line to crontab:
# @reboot /bin/bash /home/pi/app/runserver.sh


# Start pigpio daemon to gain direct access to gpio pins
sudo pigpiod

# Start server
python /home/pi/app/server.py
