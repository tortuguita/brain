""" Simple raspberry Pi board simulator for DEV environment """
import random
from shutil import copyfile
from collections import namedtuple

VERBOSE = True
PREFIX = ">> RPi Simulator:"


class Relay:

    def __init__(self):
        self.status = 0

    def read(self):
        return self.status

    def on(self):
        self.status = 1

    def off(self):
        self.status = 0


class MyFactory:
    def getGpioWrapper(self, name, port):
        return Relay()


class PiCamera:

    def start_preview(self):
        pass

    def stop_preview(self):
        pass

    def capture(self, filename):
        copyfile("testimage.jpg", filename)


class PWM:
    """ PWM Pin emulator """

    def __init__(self, pin, frequency):
        self.pin = pin
        self.frequency = frequency
        if VERBOSE:
            print(f"{PREFIX} PWM initialized on pin {pin}, frequency {frequency} Hz")

    def start(self, duty_cycle):
        if VERBOSE:
            print(f"{PREFIX} PWM {self.pin} started at duty cycle {duty_cycle}%")

    def stop(self):
        if VERBOSE:
            print(f"{PREFIX} PWM {self.pin} stopped")


class Gpio:
    """ GPIO pin set simulation """

    OUT = 1
    LOW = 0
    IN = 2

    PUD_DOWN = 0
    PUD_UP = 1

    BCM = 111
    BOARD = 222  # most probably random value

    def setmode(self, value):
        if VERBOSE:
            print(f"{PREFIX} setmode {value}")

    def setup(self, pin, value, pull_up_down=PUD_DOWN):
        if VERBOSE:
            print(f"{PREFIX} Setup pin {pin}: {value}")

    def output(self, pin, value):
        if VERBOSE:
            print(f"{PREFIX} pin {pin} set to {value}")

    def input(self, pin):
        return 0

    def cleanup(self):
        if VERBOSE:
            print(f"{PREFIX} cleanup.")

    def setwarnings(self, warnings):
        if VERBOSE:
            print(f"{PREFIX} setwarnings to {warnings}")

    def PWM(self, pin, frequency):
        return PWM(pin, frequency)


class PiGpioPi:
    """ Simulator of pigpio.pi class """
    def set_mode(self, pin_number, mode):
        pass

    def set_PWM_frequency(self, pin_number, frequency):
        pass

    def set_servo_pulsewidth(self, pin_number, width):
        print(f"Simulator: setting pigpio pin {pin_number} fo pulse width {width}")


Factory = MyFactory()
GPIO = Gpio()

PIGPIO = namedtuple("PIGPIO", "pi, OUTPUT")

pigpio = PIGPIO(PiGpioPi, 0)

print("Warning: RPi module not found, using simulator instead")
