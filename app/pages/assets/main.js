/* contains extracted parts from spaghetti main application

  CREDIT touch control app based on this work: https://github.com/CoretechR/ZeroBot
*/

var Vector2 = function (x,y) {
	this.x = x || 0;
	this.y = y || 0;
};

Vector2.prototype = {

	reset: function ( x, y ) {
		this.x = x;
		this.y = y;
		return this;
	},

	copyFrom : function (v) {
		this.x = v.x;
		this.y = v.y;
	},

	plusEq : function (v) {
		this.x+=v.x;
		this.y+=v.y;
		return this;
	},

	minusEq : function (v) {
		this.x-=v.x;
		this.y-=v.y;
		return this;
	},

	equals : function (v) {
		return((this.x==v.x)&&(this.y==v.x));
	}

};


// TODO most probably not used
function mouseOver(minX, minY, maxX, maxY){
	return(mouseX>minX&&mouseY>minY&&mouseX<maxX&&mouseY<maxY);
}


function Remap(value, from1, to1, from2, to2){
	return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
}

