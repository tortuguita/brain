""" High level rover control """
import time

try:
    import RPi.GPIO as GPIO
    import pigpio
except:
    from simulator import GPIO, PiCamera, pigpio

PWM_FREQUENCY = 1000  # in Hz

# pigpio global access to GPIO pins
# Based on https://ben.akrin.com/raspberry-pi-servo-jitter/
pigpio_pwm = pigpio.pi()


class Pin:
    """ Wrapper over on/off pin. <pin_number> is in BOARD mode. """

    def __init__(self, pin_number, mode):
        print(f"PIN: initializing {pin_number}, mode {mode}")
        self.pin_number = pin_number
        self.mode = mode
        GPIO.setup(self.pin_number, mode)
        self.status = None
        self.off()

    def on(self):
        if self.mode == GPIO.OUT:
            self.status = 1
            GPIO.output(self.pin_number, 1)

    def off(self):
        if self.mode == GPIO.OUT:
            self.status = 0
            GPIO.output(self.pin_number, 0)

    def toggle(self):
        if self.status:
            self.off()
        else:
            self.on()

    def get_value(self):
        if self.mode == GPIO.IN:
            return GPIO.input(self.pin_number)
        else:
            raise ValueError(f"Pin {self.pin_number} is not in input mode")


class PWM:
    """ Wrapper over PWM pin. <pin_number> is in BOARD mode. """

    def __init__(self, pin_number, frequency):
        print(f"PWM: Initializing {pin_number} at frequency {frequency}")
        GPIO.setup(pin_number, GPIO.OUT)
        self.pwm = GPIO.PWM(pin_number, frequency)

    def set_value(self, value):
        if value > 0:
            self.pwm.start(value)
        elif value == 0:
            self.pwm.stop()


class Motor:
    """ Motor moving forward or backward. Needs 2 pins on H-bridge """
    def __init__(self, pin_forward, pin_backward, frequency):
        self.forward = PWM(pin_forward, frequency)
        self.backward = PWM(pin_backward, frequency)

    def set_value(self, value):
        """ Value can be -100 to +100, negatives means move backward"""
        if value == 0:
            self.forward.set_value(0)
            self.backward.set_value(0)
        elif value > 0:
            self.forward.set_value(value)
            self.backward.set_value(0)
        else:
            self.forward.set_value(0)
            self.backward.set_value(abs(value))


class Servo:
    """ Wrapper over Servo
        Uses special library pigpio (using GPIO causes servos to jitter, pigpio is way more accurate)
    """

    def __init__(self, pin_number):
        self.pin_number = pin_number
        pigpio_pwm.set_mode(pin_number, pigpio.OUTPUT)
        pigpio_pwm.set_PWM_frequency(pin_number, 50)

    def set_value(self, value):
        """ Value is in range 0..100 (as percentage) """
        # Recount value to get it into 500-2500 range pigpio uses
        recounted = (value * 20) + 500
        pigpio_pwm.set_servo_pulsewidth(self.pin_number, recounted)


class Rover:

    CONNECTOR_MODE_PWM = 0
    CONNECTOR_MODE_IN = 1
    CONNECTOR_MODE_OUT = 2

    CONNECTOR_PINS = [5, 6, 13]

    def __init__(self):
        self.left_motor = Motor(27, 22, PWM_FREQUENCY)
        self.right_motor = Motor(23, 24, PWM_FREQUENCY)

        # PWM connector accessible via connector bay
        self.connectors = [None, None, None]

        # As a default, configure everything to PWMs
        self.set_connector_mode(0, self.CONNECTOR_MODE_PWM)
        self.set_connector_mode(1, self.CONNECTOR_MODE_PWM)
        self.set_connector_mode(2, self.CONNECTOR_MODE_PWM)

    def set_connector_mode(self, connector_index, mode):
        if connector_index not in range (0, 3):
            raise ValueError("Wrong connector index")

        if mode == self.CONNECTOR_MODE_PWM:
            self.connectors[connector_index] = Servo(self.CONNECTOR_PINS[connector_index])
        elif mode == self.CONNECTOR_MODE_OUT:
            self.connectors[connector_index] = Pin(self.CONNECTOR_PINS[connector_index], GPIO.OUT)
        elif mode == self.CONNECTOR_MODE_IN:
            self.connectors[connector_index] = Pin(self.CONNECTOR_PINS[connector_index], GPIO.IN)
        else:
            raise ValueError("Wrong connector mode")

    def get_ultrasonic_distance(self):
        """ Measures distance via ultrasonic sensor. I must be connected on connector 0 (trigger) and 1 (echo),
            otherwise raises error

            TODO fix wrong distance (It should be limited to something about 6 meters):
                HC-SRO4 is due to specification able to measure up to 400 cms, so everything above it
                (with some coefficient) is considered as nonsense.
                In praxis, typical nonsense distance returned, most probably due to random noise,
                was 26.5 something meters
                https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf
        """
        try:
            proper_configuration = self.connectors[0].mode == GPIO.OUT and self.connectors[1].mode == GPIO.IN  
        except AttributeError:
            proper_configuration = False
        if not proper_configuration:
            raise ValueError(f"Improperly configured pins for ultrasonic ranger")

        # send pulse 0.01ms length
        self.connectors[0].on()
        time.sleep(0.00001)
        self.connectors[0].off()

        start_time = time.time()
        stop_time = time.time()

        while self.connectors[1].get_value() == 0:
            start_time = time.time()

        while self.connectors[1].get_value() == 1:
            stop_time = time.time()

        # multiply time difference with the speed of sound(34300 cm/s) and divide by 2, (ida y vuelta)
        time_elapsed = stop_time - start_time
        distance = (time_elapsed * 34300) / 2

        return distance


# BCM mode is kind of mapping, however, there are problems initializing GPIO.BOARD mode, so BCM mode is used
GPIO.setmode(GPIO.BCM)
