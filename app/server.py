""" Tortuguita server """

import flask
from flask import Flask, redirect, send_from_directory, send_file, Response
import os
import io
import time
import logging
import math
import json

try:
    from picamera2 import Picamera2
    from libcamera import Transform
except ModuleNotFoundError:
    print("picamera2 module not found, streaming not available")

from rover import Rover

app = Flask(__name__)
cpu_serial_number = None

STATUS_FN = "./status.json"

# Hint: to get available resolutions, SSH to Pi and run ```libcamera-hello --list-cameras```
CAMERA_RESOLUTIONS = [
    (320, 240),
    (640, 480),
    (1640, 1232),
    (1920, 1080),
# This is theoretically supported by camera, but in praxis throws a "Cannot allocate memory" error on RPi Zero:
#    (3280, 2464),
]


# Instantiate robot and setup it's pins initially
rover = Rover()


class Status:
    """ Keeps status flags, that are serialized to file"""

    def __init__(self):
        self.data = {}
        self.load()

    @property
    def lr_reverse(self):
        return self.data.get("lr_reverse", False)

    @lr_reverse.setter
    def lr_reverse(self, data):
        self.data["lr_reverse"] = data
        self.save()

    @property
    def fb_reverse(self):
        return self.data.get("fb_reverse", False)

    @fb_reverse.setter
    def fb_reverse(self, data):
        self.data["fb_reverse"] = data
        self.save()

    def load(self):
        try:
            with open(STATUS_FN, "r") as f:
                self.data = json.load(f)
        except FileNotFoundError:
            pass

    def save(self):
        with open(STATUS_FN, "w") as f:
            f.write(json.dumps(self.data, indent=4))


# Global variables
is_streaming = True  # is /stream endpoint active?
power_coefficient = 1  # Used to recalculate motor power, see recalc_power function below
last_frame_io = None
status = Status()
camera_resolution_index = 0  # into the CAMERA_RESOLUTIONS
picam2 = None


@app.route('/')
def homepage():
    return send_file("pages/index.html")


@app.get('/info')
def info():
    """ Returns bot's configuration as JSON """
    return {
        "generic": {
            "name": "Tortuguita"
        },
        "bot": {
           "power_coefficient": power_coefficient,
           "cpu_serial_number": cpu_serial_number,
        },
        "camera": {
            "streaming": is_streaming,
            "resolution": CAMERA_RESOLUTIONS[camera_resolution_index],
            "available_resolutions": CAMERA_RESOLUTIONS,
        }
    }


@app.route("/assets/<path:name>")
def serve_asset(name):
    return send_from_directory(
        "pages/assets", name
    )


@app.post('/do/move')
def move():
    """ Moves motors. get arguments "left" and "right" are percentages of motor strength.
        Negative values means motor moves backwards.
    """

    def recalc_power(x):
        """ Recalculates motor input power to output, providing something like power curve.
            (low motor powers barely move the motors, so sometimes tortuguita need steeper curve
        """
        negative = (x < 0)
        ret = 100 - math.pow(1 - abs(x) / 100, power_coefficient) * 100
        if negative:
            ret = - ret
        return ret

    left = flask.request.args["left"]
    right = flask.request.args["right"]

    MAX_RANGE = 255

    # Enforce proper value range
    left = min(max(int(left), -MAX_RANGE), MAX_RANGE)
    right = min(max(int(right), -MAX_RANGE), MAX_RANGE)

    # Recalculate to PWM percentage, expressed as int number in range 0..100
    left = int(100 * left / MAX_RANGE)
    right = int(100 * right / MAX_RANGE)

    # Implement motor reverses
    if status.lr_reverse:
        left, right = right, left
    if status.fb_reverse:
        left = -left
        right = -right

    # Implement power curve
    left = recalc_power(left)
    right = recalc_power(right)

    # and power our motors
    rover.left_motor.set_value(left)
    rover.right_motor.set_value(right)
    return "ok"


@app.post('/do/lightness/<int:value>')
def set_lightness(value):
    """ Changes lightness of camera. Value can be 0..100 as a percentage of maximum"""
    if is_streaming:
        value_min, value_max = -1.0, 1.0
        size = value_max - value_min
        offset = value * size / 100
        new_value = value_min + offset
        picam2.controls.Brightness = new_value
    else:
        print(f"Setting brightness to {value}% - not implemented, no camera")
    return "ok"


@app.post('/do/pwm/<int:index>/<int:value>')
def set_connector_value(index, value):
    """ Value is in range 0..100 (as percentage) """
    if index in range(0, 3):
        rover.connectors[index].set_value(value)
        return "ok"
    else:
        return "wrong index"


@app.post('/do/shutdown')
def shutdown():
    """ Power off the computer """
    # based on https://stackoverflow.com/questions/23013274/shutting-down-computer-linux-using-python
    os.system('sudo shutdown now -h')
    return redirect("/")


@app.post('/set/<string:what>/<string:value>')
def set_variable(what, value):

    if what == "powercoef":
        global power_coefficient
        power_coefficient = float(value)
        return "OK"

    return "Do not know what you want to set", 404


@app.route('/get/<string:what>')
def get_variable(what):
    """ Return status of various variables """

    def to_bool(x):
        return "ON" if x else "OFF"

    if what == "streaming":
        return to_bool(is_streaming)
    if what == "lr_reverse":
        return to_bool(status.lr_reverse)
    if what == "fb_reverse":
        return to_bool(status.fb_reverse)
    if what == "powercoef":
        return to_bool(power_coefficient)

    return "Do not know what you want to get", 404


@app.post('/do/toggle/reverse/lr')
def toggle_lr_reverse():
    global status
    status.lr_reverse = not status.lr_reverse
    return "OK"


@app.post('/do/toggle/reverse/fb')
def toggle_fb_reverse():
    global status
    status.fb_reverse = not status.fb_reverse
    return "OK"


def gen():
    """ Video streaming generator """
    global last_frame_io  # Maybe not necessary ? Was for /img endpoint, now it is not used anymore
    while True:

        # about 65-70 msec
        jpeg = picam2.capture_image("main")

        # Image must be "saved" as JPEG into bytes array. Get use of BytesIO for this
        # Also, save this IO bytearray t global variable, so it can be used to obtain it on another endpoint
        # duration: about 10-15 msec
        last_frame_io = io.BytesIO()
        jpeg.save(last_frame_io, format='jpeg')   # this is the slow bastard
        frame = last_frame_io.getvalue()

        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/img')
def img():
    """ Returns camera image as JPEG """
    myframe = io.BytesIO()
    jpeg = picam2.capture_image("main")
    jpeg.save(myframe, format='jpeg')  # this is the slow bastard
    myframe.seek(0)  # Maybe not necessary?
    return send_file(myframe, mimetype='image/jpeg')


@app.post('/do/stream/toggle')
def stream_toggle():
    global is_streaming
    is_streaming = not is_streaming
    return "OK"


@app.post('/resolution/switch')
def switch_resolution():
    """ Rotates between camera resolution modes"""
    global camera_resolution_index

    camera_resolution_index += 1
    if camera_resolution_index >= len(CAMERA_RESOLUTIONS):
        camera_resolution_index = 0

    change_camera_resolution()
    return f"Camera resolution was set to {CAMERA_RESOLUTIONS[camera_resolution_index]}"


@app.route('/stream')
def stream():
    """Video streaming. Put /stream in the src attribute of an img tag."""
    if is_streaming:
        try:
            return Response(gen(), mimetype='multipart/x-mixed-replace; boundary=frame')
        except:
            pass

    return Response("No stream")


@app.post('/ultrasonic/set')
def set_ultrasonic():
    """ Set connectors to proper mode for ultrasonic ranger """
    rover.set_connector_mode(0, rover.CONNECTOR_MODE_OUT)
    rover.set_connector_mode(1, rover.CONNECTOR_MODE_IN)
    return "Connectors were reconfigured to ultrasonic sensor (0-TRIGGER, 1-ECHO)"


@app.route('/ultrasonic/get')
def ultrasonic():
    """ Returns distance measured by ultrasonic sensor as JSON """
    try:
        return {"distance": rover.get_ultrasonic_distance()}
    except Exception as e:
        return str(e), 404


def change_camera_resolution():
    global picam2

    try:
        picam2.stop()
    except Exception as e:
        # TODO paranoid check for 1st initialization, probably not needed (or change Exception to something narrow)
        print("Camera stop error: " + str(e))

    # Configuration highly affects frame rate, thus latency:
    #   Lower resolutions significantly enhance the speed
    #   queue .. set to True (default)
    #   do not set raw=None, causes some strange behaviour
    #   format seems to have no significant effect
    #   Do not modify sensor_mode, the default one is fine
    myconfig = picam2.create_preview_configuration(
        {"size": CAMERA_RESOLUTIONS[camera_resolution_index]},
        display=None,  # have no significant effect
        encode=None,  # have no significant effect
        buffer_count=6,  # 6 or 4 is best, 1 is slow, values above 6 seems to have no effect
        controls={
            "NoiseReductionMode": 1,  # 1 or 2 seems to be fine
            "FrameDurationLimits": (50000, 50000),  # heuristically seems to be optimal
        },
        transform=Transform(hflip=True, vflip=True)  # camera is mounted upside down, so flip view in both axis
    )
    picam2.align_configuration(myconfig)  # Not necessary, but can help fix non-efficient resolutions

    # TODO this sometimes throws off "cannot allocate memory" error.
    #  refactor so entire function returns status and camera_resolution_index is not changed, if this fails
    picam2.configure(myconfig)

    picam2.start()


def get_cpu_serial_number():
    try:
        with open("/sys/firmware/devicetree/base/serial-number", "r") as f:
            ret = f.read()

        # strip off junk from the end of the string
        if ret[-1] == '\u0000':
            ret = ret[:-1]

        return ret
    except:  # Yes, exception is too broad, but we simply does not want to crash on that
        pass


if __name__ == '__main__':

    # Make logging less verbose (e.g. supress logging individual requests)
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)

    cpu_serial_number = get_cpu_serial_number()
    print(f"CPU serial number is: '{cpu_serial_number}'")

    try:
        picam2 = Picamera2()
    except NameError:
        pass

    if picam2:
        change_camera_resolution()
    else:
        is_streaming = False

    time.sleep(.5)  # Is that even necessary?

    # Start server
    # all zeros in host are necessary to open port outside localhost
    # Port 80 is blocked (permission denied) on RPi zero, so defaulting to 5000 even in production
    host, port = "0.0.0.0", 5000
    print(f"Starting server at http://localhost:{port}")
    app.run(host=host, port=port)
