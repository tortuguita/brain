/*
    Button for setting value On/Off
    Used to control LEDs etc
*/
import React, { useEffect, useState } from "react"
import Button from './Button.js';


function toggleStreamWindow(value) {
    document.getElementById("stream").src = value ? "/stream" : "assets/title.png";
}


export default function ButtonArea() {

  return (
    <div className="arow w-100 mx-0 mb-1">
        <Button name="Streaming" title="Switch on or off camera streaming" url="/do/stream/toggle" url_get='/get/streaming' callback={toggleStreamWindow} />
        <ToggleResolutionButton/>
        <Button name="LR" title="Switch left and right motors" url="/do/toggle/reverse/lr" url_get='/get/lr_reverse' />
        <Button name="FB" title="Switch front and back motor direction" url="/do/toggle/reverse/fb" url_get='/get/fb_reverse'/>
    </div>
  );
}


function ToggleResolutionButton({}) {

    const [resolution, setResolution] = useState("")

    function get_from_server() {
        fetch("/info", {method: 'GET'}).then(response => {return response.json()}).then(body => {
            setResolution("" + body.camera.resolution[0] + "x" + body.camera.resolution[1])
        })
    }

    function toggle() {
        fetch("/resolution/switch", { method: 'POST',})
        get_from_server();
    }

    get_from_server();

    return (
        <a href="#" onClick={ toggle } title="Toggle resolution" className="btn-dark btn rounded-pill px-1 py-1 mx-0 my-1 w-100 h-100">
            Resolution {resolution}
        </a>
    );

}
