import { createRoot } from 'react-dom/client';

import App from './App.js';

try { createRoot(document.getElementById('react-app')).render(<App />); } catch {}
