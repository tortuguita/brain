/*
    Button for setting value On/Off
    Used to control LEDs etc
*/
import React, { useEffect, useState } from "react"


export default function Button({ name, url, url_get, callback, title }) {

    const [value, setValue] = useState(false)

    function update_on_server(x) {
        if(url) {
            let myUrl = url;
            try {
                fetch(myUrl, { method: 'POST',})
            } catch (e) {
            }
        }
    }

    function get_from_server() {
        if(url_get) {
            fetch(url_get, {method: 'GET'}).then(response => {return response.text()}).then(body => {
                let val = (body == "ON") ? true : false
                setValue(val)
                if (callback) {
                    callback(val)
                }
            })
        }
    }

  function toggle() {
    let new_value = !value
    setValue(new_value);
    update_on_server(new_value);
    get_from_server();
  }

  get_from_server();

  return (
        <a href="#" onClick={ toggle } title={title} className={ (value ? "btn-success" : "btn-dark") + " btn rounded-pill px-1 py-1 mx-0 my-1 w-100 h-100" }>{name}</a>
  );
}

