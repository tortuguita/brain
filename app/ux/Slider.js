/*
    Slider for setting value in some range.
    Used to control lightness, PWMs etc
*/

import { createRoot } from 'react-dom/client';
import React, { useEffect, useState } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'


export default function Slider({ name, url, initial, step=10, min=0, max=100, decimals=0 }) {

    const [value, setValue] = useState(parseInt(initial))

    function update(x) {
        if(url) {
            let myUrl = url + x.toFixed(decimals);
            try {
                fetch(myUrl, { method: 'POST',})
            } catch (e) {
            }
        }
    }

  function decrement() {
    let x = value - Number(step);
    if (x < Number(min)) {
        x = Number(min);
    }
    setValue(x);
    update(x);
  }

  function increment() {
    let x = value + Number(step);
    if (x > Number(max)) {
        x = Number(max);
    }
    setValue(x);
    update(x);
  }

  return (
       <div className="row justify-content-center align-items-center border rounded rounded-pill mb-2 mx-0 p-0">
                <div className="col-4 p-0 h-100 text-center">
                    <FontAwesomeIcon className="pointer w-100 big-2" onClick={decrement} icon={fas.faCircleMinus} />
                </div>
                <div className="col-4 p-0">
                    <div className="small text-center m-0 p-0">{name}</div>
                    <div className="big text-center">{value.toFixed(decimals)} %</div>
                </div>
                <div className="col-4 p-0 text-center">
                    <FontAwesomeIcon className="pointer w-100 big-2" onClick={increment} icon={fas.faCirclePlus} />
                </div>
       </div>
  );

}
