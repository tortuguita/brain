/*
    Entire app
*/
import React, { useEffect, useState } from "react"
import Button from './Button.js';
import Slider from './Slider.js';
import ButtonArea from './Buttons.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'


function shutdownTG() {
    if (confirm('Really shutdown Tortuguita?')) {
        try {
            fetch("/do/shutdown", { method: 'POST',})
        } catch (e) {
        }
    }
}


export default function App() {

  const [settingsOpen, setSettingsOpen] = useState(false)
  const [pwmOpen, setPwmOpen] = useState(true)

  return (
    <>
        <div className="w-100 pt-2">
            <div className="row mx-0 px-0">
                <div className="col-12 col-lg-8 p-0 pt-1 d-flex mb-2">
                    <img src="/assets/tortuguita.png" height="30" className="mr-2 pb-1" />
                    <div className="mt-1 pb-1">v0.4</div>
                </div>
                <div className="col-12 col-lg-4 p-0 mb-2 text-left text-lg-right">
                    <FontAwesomeIcon className="text-dark bg-white p-1 rounded mr-1" onClick={x=>{setPwmOpen(!pwmOpen)}} icon={fas.faSliders} />
                    <FontAwesomeIcon className="text-dark bg-white p-1 rounded mr-2" onClick={x=>{setSettingsOpen(!settingsOpen)}} icon={fas.faGear} />
                    <FontAwesomeIcon className="text-dark bg-warning p-1 rounded" onClick={shutdownTG} icon={fas.faPowerOff} />
                </div>
            </div>
        </div>

        { settingsOpen
            ? <>
                    <ButtonArea />
                    <Slider name="Power curve" url="/set/powercoef/" initial="1" step="0.1" min="1" max="4" decimals="1" />
                    <Slider name="Camera lightness" url="/do/lightness/" initial="50" />
              </>
            : ""
        }

        { pwmOpen
            ? <>
                <Slider name="PWM1" url="/do/pwm/0/" initial="0" />
                <Slider name="PWM2" url="/do/pwm/1/" initial="0" />
                <Slider name="PWM3" url="/do/pwm/2/" initial="0" />
              </>
            : ""
        }
    </>
  );
}


