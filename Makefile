PYTHON = python
RECEIVER_PORT = ttyACM0

help:
	@echo "  pipi           Import (install) requirements.txt to pip"
	@echo "  todo           dump TODO tags from project"
	@echo ""
	@echo "  test           Tests"
	@echo "  cov            Coverage report"
	@echo ""
	@echo "  r              Start rover server"
	@echo "  node           run node part of the application (auto compile sources in DEV)"
	@echo "  nodec          Compile UX (javascript) part in node for production"
	@echo ""

pipi:
	pip install -r requirements.txt
	pip install -r requirements-dev.txt

todo:
	@rgrep -EI "TODO" | \
	    grep -v "\.venv" | \
	    grep -v "Makefile" | \
	    grep -v "app/pages/assets/css/bootstrap" | \
	    grep -v "app/pages/assets/compiled" | \
	    grep -v "node_modules" | \
	    grep -v "\.git"

r:
	python -u app/server.py

node:
	npm start

nodec:
	npm run compile

test:
	pytest

cov:
	pytest --cov-config=coveragerc --cov=. --cov-report=term-missing
