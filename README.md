# Tortuguita brain

Tortuguita is open source, hackable, easy to build tracked robot, powered by Raspberry Pi Zero.  
Home page: [tortuguita.tech](https://tortuguita.tech)

This repository contains:

- Control program
- Instructions and resources how to [install operating system and software](doc/installation.md)

## License

This software is licensed under [GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.html)  

What does it mean? You can freely use, copy, distribute, study and modify this software, 
even use it in your commerce project(s), but you must keep it under GPL license.  
And also, the software comes with no warranty at all.

Other software referenced in this project (like Raspberry Pi OS), 
may use different licenses.  

## Project structure

app/                    control application  
doc/                    documentation


## Control program

Is a simple HTTP server based on Flask, with some frontend things 

### Tech stack

Backend: Python & Flask  
Frontend: React + Node.js, Bootstrap, Fontawesome icons


