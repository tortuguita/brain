# Installation

Raw set of instructions how to install operating system and software to RPi.
Assuming you are an experienced user, knowing something about software, bash and linux in general.

## Prerequisites

- Program Raspberry Pi Imager ```sudo apt install rpi-imager```
- Micro SD Card (8 GB at least, 32 GB preferred)
- Linux machine with card reader

## Install Operating system on SD card

- Insert SD card into the reader
- Run ```sudo rpi-imager```
- Select OS "Raspberry Pi OS (32 bit)"
- Advanced options:
  - Select "To always use" from top dropdown menu
  - Configure wireless LAN (SSID, password)
      - RPi Zero 2 does not support 5G WiFi networks
      - Pay attention to selecting proper Wifi country
- Select target card, burn
- Remove SD card (do not need to eject, Imager does this)

## Power up RPi

Put SD card into RPi, power up. SSH and WiFi should be enabled.

Sometimes you can experience troubles connecting to Wifi (seems to be RPi-Imager issue).
In that case, connect monitor and keyboard and set up Wi-Fi manually:

- Ctrl + Alt + T to open terminal
- ```sudo raspi-config```

## Test SSH login

We need SSH to proceed. So it's time to test it: 

    ssh -o PubkeyAuthentication=no pi@<address>

A good idea is to add your pubkey to RPi at this moment, so you do not have to manually enter credentials every time 
you connect:

    ssh-copy-id -o PubkeyAuthentication=no -i <path-to-your-public-key> pi@<address>

Test connection w/o password 

    ssh pi@<address>

## Configure GPIO pins and camera

- connect camera to RPi, power on
- ssh to RPi
- run ```sudo raspi-config``` and enable GPIO:
    - Interface options / remote gpio
- Restart RPi ```sudo reboot```
- Test camera, for example by taking still image  ```raspistill -o test-image.jpg```

## Install servo library

Tortuguita has three PWM slots in the connector bay. 
Controlling it on RPi Zero by a standard way causes [servo jittering](https://ben.akrin.com/raspberry-pi-servo-jitter/). 
To avoid this, install this great library that works with interrupts. No jittering, no problems:

    sudo apt-get update && sudo apt-get install python3-pigpio


## Camera streaming support

Control program uses default Python module [picamera2](../../3rdparty/picamera2-manual.pdf).  
On a freshly installed RP OS it sometimes does not work properly. So it is a good idea to upgrade apt libraries:  

    sudo apt-get update
    sudo apt-get full-upgrade  # This one takes about an hour

## Publish control program

Find Tortuguita in your local network and set up ENV variable ```TORTUGUITA_IP```
Than run ```./publish``` from this directory.   
This will copy control program to RPi.

## Starting control program

Program can be run manually, just to test if it works: 
- ssh to RPi
- run ```app/runserver.sh```

However, we want to run this program automatically every time RPi boots up.
It can be achieved by many different ways, here we make use of crontab:

- ssh to RPi
- run ```crontab -e```
- Add this line: ```@reboot /bin/bash /home/pi/app/runserver.sh```
- Write out, exit and restart RPi (```sudo reboot```).

## Control

Navigate your browser to:

  ```http://<tortuguita_ip_address>:5000```

You should see camera stream an be able to control motors and everything.

It is also a good idea always gracefully shutdown the system (app has a button for it) 
before switching of Tortugita. Raspberry Pi OS is pretty tolerant for sudden power-offs,
but ... you know :)



